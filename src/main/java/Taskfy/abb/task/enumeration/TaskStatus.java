package Taskfy.abb.task.enumeration;

public enum TaskStatus {

    FINISHED, ONGOING, DRAFT
}
