package Taskfy.abb.task.repository;

import Taskfy.abb.task.enumeration.RoleEnum;
import Taskfy.abb.task.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(RoleEnum name);
}
