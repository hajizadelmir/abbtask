package Taskfy.abb.task.repository;

import Taskfy.abb.task.dto.userTaskDto.TaskSearchRequest;
import Taskfy.abb.task.model.Organization;
import Taskfy.abb.task.model.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.persistence.criteria.Predicate;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long>, JpaSpecificationExecutor<Task> {

    Optional<Task> findByTitle(String title);

    default Page<Task> findAll(TaskSearchRequest searchReq) {
        return findAll((root, criteria, cb) -> {
            Predicate predicate = cb.conjunction();

            if (searchReq.getId() != null) {
                predicate = cb.and(predicate, root.get(Organization.Fields.id).in(searchReq.getId()));
            }
            if (searchReq.getName() != null) {
                predicate = cb.and(predicate, root.get(Task.Fields.description).in(searchReq.getName()));
            }
            return predicate;
        }, PageRequest.of(searchReq.getPage(), searchReq.getSize(), Sort.by(Sort.Direction.valueOf(searchReq.getDir()), searchReq.getOrder())));
    }
}
