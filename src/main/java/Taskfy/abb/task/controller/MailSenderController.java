package Taskfy.abb.task.controller;

import Taskfy.abb.task.service.MailSenderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/mailSender")
public class MailSenderController {

    private final MailSenderService service;

    @GetMapping
    void sendEmail() {
        service.sendWithoutAttachment("Orxanmamedov214@gmail.com", "hajizadelmir@gmail.com", "test");
    }
}
