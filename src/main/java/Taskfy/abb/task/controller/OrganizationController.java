package Taskfy.abb.task.controller;

import Taskfy.abb.task.dto.organizationDto.AddUserDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationInfoDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationSearch;
import Taskfy.abb.task.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("organizations")
@RequiredArgsConstructor
public class OrganizationController {

    private final OrganizationService organizationService;

    @GetMapping
    public Page<OrganizationInfoDto> list(@RequestBody OrganizationSearch searchRequest) {
        return organizationService.list(searchRequest);
    }

    @PostMapping
    public ResponseEntity<OrganizationInfoDto> add(@RequestBody @Valid OrganizationDto requestDto) {
        log.trace("Create OrganizationResponseInfoDto request {}", requestDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(organizationService.create(requestDto));
    }


    @DeleteMapping("/organization/{organizationId}")
    public ResponseEntity<Void> deleteOrganizationUser(@PathVariable Long organizationId, @RequestBody @Valid AddUserDto registerOrganizationUser) {
        log.trace("Delete OrganizationUser {},{}  request", organizationId, registerOrganizationUser);
        organizationService.removeUserFromOrganization(registerOrganizationUser, organizationId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/organization/{organizationId}")
    public ResponseEntity<Void> addOrganizationUsers(@PathVariable Long organizationId, @RequestBody @Valid AddUserDto registerOrganizationUser) {
        log.trace("Add GroupUser  {},{}  request", organizationId, registerOrganizationUser);
        organizationService.addUserToOrganization(registerOrganizationUser, organizationId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
