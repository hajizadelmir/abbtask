package Taskfy.abb.task.controller;

import Taskfy.abb.task.dto.LoginDto;
import Taskfy.abb.task.dto.RegisterDto;
import Taskfy.abb.task.exception.EmailAlreadyExistException;
import Taskfy.abb.task.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.management.relation.RoleNotFoundException;
import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private final AuthenticationService service;

    @PostMapping("/signUp")
    public void signUpUser(@Valid @RequestBody RegisterDto registerRequest) throws RoleNotFoundException, EmailAlreadyExistException {
        service.signUp(registerRequest);
    }

    @PostMapping("/signIn")
    public ResponseEntity<?> signInUser(@Valid @RequestBody LoginDto loginRequest) {
        return service.signIn(loginRequest);
    }
}
