package Taskfy.abb.task.controller;

import Taskfy.abb.task.dto.userTaskDto.TaskRequest;
import Taskfy.abb.task.dto.userTaskDto.TaskResponse;
import Taskfy.abb.task.dto.userTaskDto.TaskSearchRequest;
import Taskfy.abb.task.dto.userTaskDto.TaskSearchResponseInfo;
import Taskfy.abb.task.service.UsertaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/userTasks")
public class UserTaskController {

    private final UsertaskService usertaskService;

    @GetMapping
    public Page<TaskSearchResponseInfo> taskList(@RequestBody TaskSearchRequest searchRequest) {
        return usertaskService.taskList(searchRequest);
    }

    @PostMapping("/create")
    public ResponseEntity<TaskResponse> createTask(@RequestBody @Valid TaskRequest requestDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(usertaskService.createTask(requestDto));
    }

    @PostMapping("/{taskId}/user/{userId}")
    public ResponseEntity<Void> taskAddForUser(@PathVariable Long taskId, @PathVariable Long userId) {
        log.trace("Added user task");
        usertaskService.taskAddForUser(taskId, userId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTask(@PathVariable Long id) {
        log.trace("Delete task with id {}", id);
        usertaskService.deleteTask(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
