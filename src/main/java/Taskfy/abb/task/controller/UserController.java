package Taskfy.abb.task.controller;

import Taskfy.abb.task.dto.RegisterDto;
import Taskfy.abb.task.dto.userDto.RegisterUser;
import Taskfy.abb.task.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserServiceImpl userService;

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody @Validated RegisterUser dto) {
        userService.create(dto);
        return ResponseEntity.ok().build();
    }
}
