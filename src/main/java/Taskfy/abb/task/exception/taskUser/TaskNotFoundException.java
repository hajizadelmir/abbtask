package Taskfy.abb.task.exception.taskUser;

import Taskfy.abb.task.exception.NotFoundException;

public class TaskNotFoundException extends NotFoundException {

    public static final String MESSAGE = "Task %s does not exist.";

    private static final long serialVersionUID = 154549132465811L;

    public TaskNotFoundException(Long taskId) {
        super(String.format(MESSAGE, taskId));
    }
}
