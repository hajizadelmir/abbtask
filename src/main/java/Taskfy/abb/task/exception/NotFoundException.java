package Taskfy.abb.task.exception;

public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 154549132465811L;

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable ex) {
        super(message, ex);
    }
}
