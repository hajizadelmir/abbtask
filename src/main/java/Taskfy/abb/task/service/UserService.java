package Taskfy.abb.task.service;

import Taskfy.abb.task.dto.userDto.RegisterUser;

public interface UserService {

    void create(RegisterUser dto);
}
