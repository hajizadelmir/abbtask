package Taskfy.abb.task.service;

import Taskfy.abb.task.dto.LoginDto;
import Taskfy.abb.task.dto.RegisterDto;
import Taskfy.abb.task.exception.EmailAlreadyExistException;
import Taskfy.abb.task.model.User;
import org.springframework.http.ResponseEntity;

import javax.management.relation.RoleNotFoundException;

public interface AuthenticationService {
    User signUp(RegisterDto registerDto) throws RoleNotFoundException, EmailAlreadyExistException;

    ResponseEntity<?> signIn(LoginDto loginRequest);
}
