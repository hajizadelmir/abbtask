package Taskfy.abb.task.service;

public interface MailSenderService {

    void sendWithoutAttachment(String to, String sender, String text);
}
