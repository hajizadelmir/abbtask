package Taskfy.abb.task.service;

import Taskfy.abb.task.dto.userTaskDto.TaskRequest;
import Taskfy.abb.task.dto.userTaskDto.TaskResponse;
import Taskfy.abb.task.dto.userTaskDto.TaskSearchRequest;
import Taskfy.abb.task.dto.userTaskDto.TaskSearchResponseInfo;
import org.springframework.data.domain.Page;

public interface UsertaskService {

    TaskResponse createTask(TaskRequest taskRequest);

    void taskAddForUser(Long taskId, Long userid);

    void deleteTask(Long id);

    Page<TaskSearchResponseInfo> taskList(TaskSearchRequest searchRequest);
}
