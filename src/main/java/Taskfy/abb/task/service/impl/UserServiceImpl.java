package Taskfy.abb.task.service.impl;

import Taskfy.abb.task.dto.userDto.RegisterUser;
import Taskfy.abb.task.enumeration.RoleEnum;
import Taskfy.abb.task.exception.EmailAlreadyExistException;
import Taskfy.abb.task.model.Role;
import Taskfy.abb.task.model.User;
import Taskfy.abb.task.repository.RoleRepository;
import Taskfy.abb.task.repository.UserRepository;
import Taskfy.abb.task.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    @Override
    public void create(RegisterUser dto) {
        userRepository.findByEmail(dto.getEmail())
                .ifPresent(user -> {
                    throw new EmailAlreadyExistException();
                });
        User user = createUserEntityObject(dto);
        userRepository.save(user);

    }

    private User createUserEntityObject(RegisterUser registerUser) {

        User user = new User();
        user.setPassword(passwordEncoder.encode(registerUser.getPassword()));
        Set<Role> roleEntities = new HashSet<>();
        Role userRole = roleRepository.findByName(RoleEnum.USER)
                .orElseThrow(() -> new RuntimeException("Not found Authority"));
        roleEntities.add(userRole);
        user.setRoles(roleEntities);
        user.setName(registerUser.getName());
        user.setEmail(registerUser.getEmail());
        user.setPhoneNumber(registerUser.getPhoneNumber());
        return user;
    }
}
