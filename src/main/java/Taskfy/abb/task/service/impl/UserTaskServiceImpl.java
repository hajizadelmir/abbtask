package Taskfy.abb.task.service.impl;

import Taskfy.abb.task.dto.userTaskDto.TaskRequest;
import Taskfy.abb.task.dto.userTaskDto.TaskResponse;
import Taskfy.abb.task.dto.userTaskDto.TaskSearchRequest;
import Taskfy.abb.task.dto.userTaskDto.TaskSearchResponseInfo;
import Taskfy.abb.task.exception.organizationException.OrganizationNotFoundException;
import Taskfy.abb.task.exception.taskUser.NameMustBeUniqueException;
import Taskfy.abb.task.exception.taskUser.TaskNotFoundException;
import Taskfy.abb.task.model.Organization;
import Taskfy.abb.task.model.Task;
import Taskfy.abb.task.model.User;
import Taskfy.abb.task.repository.OrganizationRepository;
import Taskfy.abb.task.repository.TaskRepository;
import Taskfy.abb.task.repository.UserRepository;
import Taskfy.abb.task.service.MailSenderService;
import Taskfy.abb.task.service.UsertaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserTaskServiceImpl implements UsertaskService {

    private final UserRepository userRepository;

    private final OrganizationRepository organizationRepository;

    private final ModelMapper modelMapper;

    private final TaskRepository taskRepository;

    private final MailSenderService mailService;

    @Override
    public TaskResponse createTask(TaskRequest taskRequest) {
        Task task = new Task();
        taskRepository.findByTitle(taskRequest.getTitle())
                .ifPresent(organization -> {
                    throw new NameMustBeUniqueException(taskRequest.getTitle());
                });
        task.setTitle(taskRequest.getTitle());
        task.setDescription(taskRequest.getDescription());
        task.setDeadline(taskRequest.getDeadline());
        task.setStatus(taskRequest.getStatus());
        task.setOrganization(getOrganisation(taskRequest.getOrganizationId()));
        return modelMapper.map(taskRepository.save(task), TaskResponse.class);

    }

    @Override
    public void taskAddForUser(Long taskId, Long userid) {

        Task task1 = taskRepository.getById(taskId);
        User user = userRepository.getById(userid);
        task1.getAssignee().add(user);
        userRepository.save(user);
        mailService.sendWithoutAttachment(user.getEmail(), "hajizadelmir@gmail.com", "{} is assigned to you. " + task1.getTitle());
    }

    @Override
    @Transactional
    public void deleteTask(Long id) {
        log.trace("Delete task with id {}", id);
        taskRepository.delete(taskRepository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException(id)));
    }

    @Override
    public Page<TaskSearchResponseInfo> taskList(TaskSearchRequest searchRequest) {
        if (searchRequest.getOrder() == null) {
            searchRequest.setOrder("id");
        }
        Page<Task> organizations = taskRepository.findAll(searchRequest);
        return organizations.map(TaskSearchResponseInfo::getDto);
    }

    private Organization getOrganisation(Long organizationId) {
        return organizationRepository.findById(organizationId)
                .orElseThrow(OrganizationNotFoundException::new);
    }
}
