package Taskfy.abb.task.service.impl;

import Taskfy.abb.task.service.MailSenderService;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import java.util.Properties;

@RequiredArgsConstructor
@Service
public class MailSenderServiceImpl implements MailSenderService {

    private static final String TASK_ASSIGNMENT = "Task Assignment";

    private final JavaMailSender javaMailSender;

    @Override
    public void sendWithoutAttachment(String to, String sender, String text) {
        JavaMailSenderImpl mailSender = javaMailSender();
        SimpleMailMessage msg = new SimpleMailMessage();

        msg.setTo(to);
        msg.setSubject(TASK_ASSIGNMENT);
        msg.setText(text);

        mailSender.send(msg);
    }

    private JavaMailSenderImpl javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("hajizadelmir@gmail.com");
        mailSender.setPassword("ntpapjnipghqedyk");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        return mailSender;
    }
}
