package Taskfy.abb.task.service.impl;

import Taskfy.abb.task.dto.organizationDto.AddUserDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationInfoDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationSearch;
import Taskfy.abb.task.exception.organizationException.OrganizationAlreadyExistException;
import Taskfy.abb.task.exception.organizationException.OrganizationNotFoundException;
import Taskfy.abb.task.mapper.OrganizationMapper;
import Taskfy.abb.task.model.Organization;
import Taskfy.abb.task.model.User;
import Taskfy.abb.task.repository.OrganizationRepository;
import Taskfy.abb.task.repository.UserRepository;
import Taskfy.abb.task.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final OrganizationMapper organizationMapper;
    private final UserRepository userRepository;

    @Override
    public OrganizationInfoDto create(OrganizationDto requestDto) {
        organizationRepository.findByName(requestDto.getName())
                .ifPresent(organization -> {
                    throw new OrganizationAlreadyExistException();
                });

        Organization organization = new Organization();
        organization.setName(requestDto.getName());
        organizationRepository.save(organization);

        OrganizationInfoDto organizationDto = OrganizationInfoDto.builder()
                .id(organization.getId())
                .name(organization.getName())
                .build();
        return organizationDto;
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void addUserToOrganization(AddUserDto registerOrgUser, Long id) {
        Organization organization = organizationFindById(id);
        checkUserIsAlreadyInTheOrg(organization, registerOrgUser.getUserId());
        User user = userFindById(registerOrgUser.getUserId());
        List<User> users = organization.getUser();
        users.add(user);
        organization.setUser(users);
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    @Override
    public void removeUserFromOrganization(AddUserDto registerOrgUser, Long id) {
        Organization organization = organizationFindById(id);
        User user = userFindById(registerOrgUser.getUserId());
        List<User> users = organization.getUser();
        users.remove(user);
        organization.setUser(users);
    }

    @Override
    public Page<OrganizationInfoDto> list(OrganizationSearch searchRequest) {
        if (searchRequest.getOrder() == null) {
            searchRequest.setOrder("id");
        }
        Page<Organization> organizations = organizationRepository.findAll(searchRequest);
        return organizations.map(OrganizationInfoDto::getDto);
    }

    private Organization organizationFindById(Long id) {
        return organizationRepository.findById(id)
                .orElseThrow(() -> new OrganizationNotFoundException(id));
    }

    private User userFindById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    private void checkUserIsAlreadyInTheOrg(Organization organization, Long userId) {
        Set<User> test = organization.getUser().stream()
                .filter(u -> u.getId() == userId)
                .collect(Collectors.toSet());
        if (!test.isEmpty()) {
            throw new OrganizationAlreadyExistException();
        }
    }
}
