package Taskfy.abb.task.service.impl;

import Taskfy.abb.task.dto.LoginDto;
import Taskfy.abb.task.dto.RegisterDto;
import Taskfy.abb.task.dto.token.TokenResponse;
import Taskfy.abb.task.enumeration.RoleEnum;
import Taskfy.abb.task.exception.EmailAlreadyExistException;
import Taskfy.abb.task.exception.EmailOrPasswordInvalid;
import Taskfy.abb.task.model.Organization;
import Taskfy.abb.task.model.Role;
import Taskfy.abb.task.model.User;
import Taskfy.abb.task.repository.OrganizationRepository;
import Taskfy.abb.task.repository.RoleRepository;
import Taskfy.abb.task.repository.UserRepository;
import Taskfy.abb.task.security.JwtUtils;
import Taskfy.abb.task.security.UserDetailsImpl;
import Taskfy.abb.task.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.management.relation.RoleNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final JwtUtils jwtUtils;
    private final OrganizationRepository organizationRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    AuthenticationServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, JwtUtils jwtUtils, OrganizationRepository organizationRep, OrganizationRepository organizationRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtils = jwtUtils;
        this.organizationRepository = organizationRepository;
    }

    @Override
    public User signUp(RegisterDto registerDto) throws RoleNotFoundException, EmailAlreadyExistException {

        User user = new User(registerDto.getUsername(),
                registerDto.getEmail(),
                passwordEncoder.encode(registerDto.getPassword()));
        Set<Role> roles = new HashSet<>();

        List<Role> roleList = roleRepository.findAll();
        if (roleList.isEmpty()) {
            roleRepository.save(Role.builder().name(RoleEnum.ADMIN).build());
            roleRepository.save(Role.builder().name(RoleEnum.USER).build());
        }
        if (userRepository.existsByEmail(registerDto.getEmail())) {
            throw new EmailAlreadyExistException();
        }
        if (registerDto.getAuthority().contains("ADMIN")) {
            Role admin = roleRepository.findByName(RoleEnum.ADMIN)
                    .orElseThrow(RoleNotFoundException::new);
            roles.add(admin);
        }
        if (registerDto.getAuthority().contains("USER")) {
            Role userRole = roleRepository.findByName(RoleEnum.USER)
                    .orElseThrow(RoleNotFoundException::new);
            roles.add(userRole);
        }

        Organization organization = new Organization(registerDto.getOrganizationName(), user);
        organizationRepository.save(organization);
        user.setRoles(roles);
        user.setOrganization(organization);
        user.setPhoneNumber(registerDto.getPhoneNumber());
        userRepository.save(user);
        return user;
    }

    @Override
    public ResponseEntity<?> signIn(LoginDto loginDto) {
        User byEmail = userRepository.findByEmail(loginDto.getEmail()).orElseThrow(EmailOrPasswordInvalid::new);
        boolean matches = passwordEncoder.matches(loginDto.getPassword(), byEmail.getPassword());
        if (!matches) {
            throw new EmailOrPasswordInvalid();
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        List<String> authorities = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new TokenResponse(jwt,
                userDetails.getId(),
                userDetails.getEmail(),
                authorities));
    }
}
