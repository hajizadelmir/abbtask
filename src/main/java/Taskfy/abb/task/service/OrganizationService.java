package Taskfy.abb.task.service;

import Taskfy.abb.task.dto.organizationDto.AddUserDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationInfoDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationSearch;
import org.springframework.data.domain.Page;

public interface OrganizationService {
    OrganizationInfoDto create(OrganizationDto organizationDto);

    void addUserToOrganization(AddUserDto addUserDto, Long id);

    void removeUserFromOrganization(AddUserDto addUserDto, Long id);

    Page<OrganizationInfoDto> list(OrganizationSearch organizationSearch);
}
