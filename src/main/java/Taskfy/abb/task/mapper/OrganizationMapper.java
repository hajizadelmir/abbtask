package Taskfy.abb.task.mapper;

import Taskfy.abb.task.dto.organizationDto.OrganizationDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationInfoDto;
import Taskfy.abb.task.model.Organization;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface OrganizationMapper {

    Organization requestToOrganization(OrganizationDto organizationRequestDto);

    OrganizationInfoDto organizationToDto(Organization organization);
}
