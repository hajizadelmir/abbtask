package Taskfy.abb.task.mapper;

import Taskfy.abb.task.dto.userDto.UserDto;
import Taskfy.abb.task.model.User;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface UserMapper {

    UserDto entityToDto(User user);
}
