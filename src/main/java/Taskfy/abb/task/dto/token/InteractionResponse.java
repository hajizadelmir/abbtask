package Taskfy.abb.task.dto.token;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InteractionResponse {

    private String inter;
}
