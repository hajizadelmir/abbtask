package Taskfy.abb.task.dto;

import Taskfy.abb.task.annotation.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDto {

    private String username;
    private String phoneNumber;

    @NotBlank
    @Size(max = 30)
    @Email
    private String email;

    @NotBlank
    @ValidPassword
    private String password;

    private List<String> authority;

    private String organizationName;
}
