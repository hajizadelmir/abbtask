package Taskfy.abb.task.dto.userDto;

import Taskfy.abb.task.annotation.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterUser {
    @NotNull
    private String name;

    @Email
    @NotNull
    private String email;

    @ValidPassword
    private String password;

    private String phoneNumber;
}
