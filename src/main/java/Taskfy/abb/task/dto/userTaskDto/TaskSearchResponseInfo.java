package Taskfy.abb.task.dto.userTaskDto;


import Taskfy.abb.task.model.Task;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskSearchResponseInfo {

    private Long id;

    private String name;

    public static TaskSearchResponseInfo getDto(Task task) {
        return TaskSearchResponseInfo.builder()
                .id(task.getId())
                .name(task.getDescription())
                .build();
    }
}
