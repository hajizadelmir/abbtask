package Taskfy.abb.task.dto.userTaskDto;


import Taskfy.abb.task.enumeration.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskRequest {

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @Valid
    private TaskStatus status;

    private LocalDate deadline;

    private Long organizationId;
}
