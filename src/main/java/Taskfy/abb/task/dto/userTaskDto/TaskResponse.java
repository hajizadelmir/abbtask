package Taskfy.abb.task.dto.userTaskDto;

import Taskfy.abb.task.dto.organizationDto.OrganizationInfoDto;
import Taskfy.abb.task.enumeration.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskResponse {

    private Long id;
    private String title;
    private String description;
    private TaskStatus status;
    private LocalDate deadline;
    private OrganizationInfoDto organization;
}
