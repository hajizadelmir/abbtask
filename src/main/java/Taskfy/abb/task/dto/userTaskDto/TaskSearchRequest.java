package Taskfy.abb.task.dto.userTaskDto;

import Taskfy.abb.task.dto.organizationDto.PageParams;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TaskSearchRequest extends PageParams {

    private Long id;

    private String name;
}
