package Taskfy.abb.task.dto.organizationDto;

import Taskfy.abb.task.model.Organization;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationInfoDto {
    private Long id;
    private String name;

    public static OrganizationInfoDto getDto(Organization organization) {
        return OrganizationInfoDto.builder()
                .id(organization.getId())
                .name(organization.getName())
                .build();
    }
}
