package Taskfy.abb.task.config;

import Taskfy.abb.task.security.AuthEntryPointJwt;
import Taskfy.abb.task.security.JwtUtils;
import Taskfy.abb.task.security.UserDetailsImpl;
import org.springframework.context.annotation.Bean;

public class TestControllerConfig {

    @Bean
    public UserDetailsImpl controllerService() {
        return new UserDetailsImpl(null, null, null, null);
    }

    @Bean
    public AuthEntryPointJwt bean() {
        return new AuthEntryPointJwt();
    }

    @Bean
    public JwtUtils jwtUtils() {
        return new JwtUtils();
    }
}
