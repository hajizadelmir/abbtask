package Taskfy.abb.task.controller;

import Taskfy.abb.task.config.TestControllerConfig;
import Taskfy.abb.task.dto.LoginDto;
import Taskfy.abb.task.dto.RegisterDto;
import Taskfy.abb.task.dto.token.InteractionResponse;
import Taskfy.abb.task.model.Organization;
import Taskfy.abb.task.model.User;
import Taskfy.abb.task.repository.OrganizationRepository;
import Taskfy.abb.task.repository.RoleRepository;
import Taskfy.abb.task.repository.UserRepository;
import Taskfy.abb.task.security.UserDetailsServiceImpl;
import Taskfy.abb.task.service.impl.AuthenticationServiceImpl;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.management.relation.RoleNotFoundException;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
@WebMvcTest(AuthenticationController.class)
@Import({TestControllerConfig.class})
public class AuthControllerTest {

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private AuthenticationServiceImpl authService;

    @Mock
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private RoleRepository authRepository;

    private Organization organization;

    private LoginDto loginCreateRequestDto;
    private RegisterDto registerRequestDto;
    private User user;

    @Mock
    Set<User> mocUsers;

    private static final String DUMMY_STRING = "elmir";
    private static final String EMAIL = "hajizadelmir@gmail.com";
    private static final Long DUMMY_ID = 1L;
    private static final String ROLE_ADMIN = "ADMIN";


    @Before
    public void setup() {
        user = User.builder()
                .id(DUMMY_ID)
                .password(DUMMY_STRING)
                .email(EMAIL)
                .name(DUMMY_STRING)
                .organization(Organization.builder()
                        .name(DUMMY_STRING)
                        .build())
                .build();

        organization = Organization.builder()
                .name(DUMMY_STRING)
                .build();

        registerRequestDto = RegisterDto.builder()
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .organizationName(DUMMY_STRING)
                .authority(List.of("ADMIN"))
                .email(EMAIL)
                .build();

        loginCreateRequestDto =LoginDto.builder()
                .email(DUMMY_STRING)
                .password(DUMMY_STRING)
                .build();
    }


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    public void registerUser() throws RoleNotFoundException {
        ResponseEntity<InteractionResponse> ok = ResponseEntity.ok(new InteractionResponse("User signUp successfully!"));
        AuthenticationController authController = new AuthenticationController(authService);
        authController.signUpUser(registerRequestDto);

        //Arrange
        when(authService.signUp(registerRequestDto)).thenReturn(user);

        //Assert
    }

    @Test
    public void loginForm_ShouldIncludeNewUserInModel() throws Exception {
        AuthenticationController authController = new AuthenticationController(authService);
        authController.signInUser(loginCreateRequestDto);

        when(authService.signIn(loginCreateRequestDto)).thenReturn(null);
    }
}
