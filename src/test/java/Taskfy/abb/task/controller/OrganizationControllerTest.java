package Taskfy.abb.task.controller;

import Taskfy.abb.task.config.TestControllerConfig;
import Taskfy.abb.task.dto.organizationDto.AddUserDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationInfoDto;
import Taskfy.abb.task.security.UserDetailsServiceImpl;
import Taskfy.abb.task.service.OrganizationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
@WebMvcTest(OrganizationController.class)
@Import({TestControllerConfig.class})
public class OrganizationControllerTest {

    private static final String MAIN_URL = "/organizations";

    private static final String NAME = "ABB";

    private static final Long DUMMY_ID = 1L;

    private static final String ROLE_ADMIN = "ADMIN";


    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private OrganizationService orgService;

    @Mock
    private ObjectMapper mapper;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @Mock
    private OrganizationDto organizationCreateRequestDto;
    private OrganizationInfoDto orgResponseInfoDto;
    private AddUserDto addUserToOrgDto;


    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();

        organizationCreateRequestDto = getOrgRequestDto();

        orgResponseInfoDto = getOrgResponseInfoDto();

        addUserToOrgDto = getRegisterOrgUse();

    }


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    public void givenRegisterOrgRequestDtoWhenCreateOrgThenSuccess() {
        //Arrange
        when(orgService.create(organizationCreateRequestDto)).thenReturn(orgResponseInfoDto);

    }


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidIdWhenDeleteOrgUserThenNoContent() {
        //Arrange
        doNothing().when(orgService).removeUserFromOrganization(addUserToOrgDto, DUMMY_ID);


    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidIdWhenAddOrgUserThenCreated() throws Exception {
        //Arrange
        doNothing().when(orgService).addUserToOrganization(addUserToOrgDto, DUMMY_ID);

        //Act
        mockMvc.perform(post(MAIN_URL + "/user/" + DUMMY_ID));


    }

    private String convertObjectToString(Object obj) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }


    private AddUserDto getRegisterOrgUse() {
        return AddUserDto.builder()
                .userId(1L)
                .build();
    }

    private OrganizationInfoDto getOrgResponseInfoDto() {
        return OrganizationInfoDto.builder()
                .id(1L)
                .name(NAME)
                .build();
    }

    private OrganizationDto getOrgRequestDto() {
        return OrganizationDto.builder()
                .name(NAME)
                .build();
    }
}
