package Taskfy.abb.task.controller;

import Taskfy.abb.task.config.TestControllerConfig;
import Taskfy.abb.task.dto.organizationDto.OrganizationInfoDto;
import Taskfy.abb.task.dto.userTaskDto.TaskRequest;
import Taskfy.abb.task.dto.userTaskDto.TaskResponse;
import Taskfy.abb.task.enumeration.TaskStatus;
import Taskfy.abb.task.security.UserDetailsServiceImpl;
import Taskfy.abb.task.service.UsertaskService;
import Taskfy.abb.task.service.impl.UserTaskServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDate;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(UserTaskController.class)
@Import({TestControllerConfig.class})
public class UserTaskControllerTest {
    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final String BASE_URL = "/tasks";
    private static final String ROLE_ADMIN = "ADMIN";
    private static final LocalDate DUMMY_DATE = LocalDate.parse("2015-02-20");


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UsertaskService taskService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    private TaskResponse taskResponseDto;
    private TaskRequest taskCreateRequestDto;

    @BeforeEach
    void setUp() {

        taskResponseDto = TaskResponse
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(LocalDate.from(DUMMY_DATE))
                .organization(OrganizationInfoDto.builder()
                        .id(DUMMY_ID)
                        .name(DUMMY_STRING)
                        .build())
                .build();

        taskCreateRequestDto = TaskRequest
                .builder()
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATE)
                .organizationId(DUMMY_ID)
                .build();
    }


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidInputWhenCreateThenReturnOk() throws Exception {
        //Arrange
        when(taskService.createTask(taskCreateRequestDto)).thenReturn(taskResponseDto);

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void givenValidIdWhenDeleteThenNoContent() throws Exception {
        //Action
        ResultActions actions = mockMvc.perform(delete(BASE_URL + "/" + DUMMY_ID)
                .contentType(MediaType.APPLICATION_JSON));

        //Assert
        verify(taskService, times(1)).deleteTask(DUMMY_ID);
        actions.andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidInputWhenSearchThenOk() throws Exception {
        //Arrange
        final Page<TaskResponse> payerDtoPage = Page.empty();

        //Act
        mockMvc.perform(post(BASE_URL + "/search")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(payerDtoPage)));
    }
}
