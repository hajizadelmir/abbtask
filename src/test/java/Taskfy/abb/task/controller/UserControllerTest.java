package Taskfy.abb.task.controller;

import Taskfy.abb.task.config.TestControllerConfig;
import Taskfy.abb.task.dto.userDto.RegisterUser;
import Taskfy.abb.task.dto.userDto.UserDto;
import Taskfy.abb.task.model.Organization;
import Taskfy.abb.task.model.User;
import Taskfy.abb.task.repository.OrganizationRepository;
import Taskfy.abb.task.repository.RoleRepository;
import Taskfy.abb.task.repository.UserRepository;
import Taskfy.abb.task.security.UserDetailsServiceImpl;
import Taskfy.abb.task.service.impl.AuthenticationServiceImpl;
import Taskfy.abb.task.service.impl.UserServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.ALL;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@Import({TestControllerConfig.class})
public class UserControllerTest {

    private static final String MAIN_URL = "/user";
    private static final String CHARACTER_ENCODING_FORMAT = "UTF-8";
    private static final String EMAIL_VALUE = "mail@mail.ru";
    private static final String NAME = "First";
    private static final String SURNAME = "Last";
    private static final String PASSWORD = "102030";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private UserServiceImpl userService;


    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;
    private RegisterUser registerUserDto;
    private UserDto userDto;

    private static final String ROLE_ADMIN = "ADMIN";

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();
        registerUserDto = RegisterUser.builder()
                .email(EMAIL_VALUE)
                .name(NAME)
                .password(PASSWORD)
                .build();
        userDto = UserDto.builder()
                .email(EMAIL_VALUE)
                .build();

    }

    @Test
    @WithMockUser(roles = ROLE_ADMIN)
    public void givenRegisterUserDtoWhenCreateUserThenSuccess() throws Exception {

        //Act
        mockMvc.perform(post(MAIN_URL)
                        .contentType(APPLICATION_JSON)
                        .characterEncoding(CHARACTER_ENCODING_FORMAT)
                        .content(convertObjectToString(registerUserDto))
                        .accept(ALL))
                .andExpect(status().isOk());
    }

    private String convertObjectToString(Object obj) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }


}
