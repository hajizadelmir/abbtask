package Taskfy.abb.task.controller;

import Taskfy.abb.task.security.AuthEntryPointJwt;
import Taskfy.abb.task.security.JwtUtils;
import Taskfy.abb.task.security.UserDetailsServiceImpl;
import Taskfy.abb.task.service.impl.MailSenderServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(MailSenderController.class)
public class MailControllerTest {

    @MockBean
    private MailSenderServiceImpl mailService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private JwtUtils jwtUtils;

    @Test
    void mailController() {
        verify(mailService, times(0)).sendWithoutAttachment("Orxanmamedov214@gmail.com", "hajizadelmir@gmail.com", "testContent");
    }
}
