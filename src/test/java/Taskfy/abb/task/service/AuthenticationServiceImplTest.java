package Taskfy.abb.task.service;

import Taskfy.abb.task.dto.LoginDto;
import Taskfy.abb.task.dto.RegisterDto;
import Taskfy.abb.task.enumeration.RoleEnum;
import Taskfy.abb.task.exception.EmailOrPasswordInvalid;
import Taskfy.abb.task.model.Organization;
import Taskfy.abb.task.model.Role;
import Taskfy.abb.task.model.User;
import Taskfy.abb.task.repository.OrganizationRepository;
import Taskfy.abb.task.repository.RoleRepository;
import Taskfy.abb.task.repository.UserRepository;
import Taskfy.abb.task.security.JwtUtils;
import Taskfy.abb.task.service.impl.AuthenticationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.management.relation.RoleNotFoundException;
import javax.servlet.Filter;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceImplTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository authRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtUtils jwtUtils;

    @Mock
    private Authentication authentication;
    @Mock
    private OrganizationRepository organizationRep;
    @InjectMocks
    private AuthenticationServiceImpl authenticationService;

    @Mock
    AuthenticationManager authenticationManager;

    private Set<User> mocUser;

    private User user;
    private Organization organization;

    private RegisterDto registerDto;

    private LoginDto loginDto;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;

    private MockMvc mvc;

    private static final String DUMMY_STRING = "0505000000";
    private static final String EMAIL = "test@test.com";
    private static final Long DUMMY_ID = 1L;
    private static final String ROLE_ADMIN = "ADMIN";

    @BeforeEach
    public void setUp() {
        user = User.builder()
                .phoneNumber(DUMMY_STRING)
                .id(DUMMY_ID)
                .password(DUMMY_STRING)
                .email(EMAIL)
                .name(DUMMY_STRING)
                .organization(Organization.builder()
                        .name(DUMMY_STRING)
                        .build())
                .build();
        organization = Organization.builder()
                .name(DUMMY_STRING)
                .build();
        registerDto = RegisterDto.builder()
                .phoneNumber(DUMMY_STRING)
                .password(DUMMY_STRING)
                .organizationName(DUMMY_STRING)
                .authority(List.of(ROLE_ADMIN))
                .email(DUMMY_STRING)
                .build();
        loginDto = LoginDto.builder()
                .email(EMAIL)
                .password(DUMMY_STRING)
                .build();

    }


    @Test
    public void registerUser() throws RoleNotFoundException {
        when(passwordEncoder.encode(any())).thenReturn(DUMMY_STRING);
        when(userRepository.existsByEmail(any())).thenReturn(false);
        when(authRepository.findByName(any())).thenReturn(Optional.of(Role.builder()
                .name(RoleEnum.ADMIN)
                .build()));
        when(userRepository.save(any())).thenReturn(user);
        when(organizationRep.save(any())).thenReturn(organization);

        User user = authenticationService.signUp(registerDto);

        assertThat(user.getEmail()).isEqualTo(DUMMY_STRING);
    }


    @Test
    public void login() throws Exception {


        assertThatThrownBy(() -> authenticationService.signIn(loginDto)).isInstanceOf(EmailOrPasswordInvalid.class);

        mvc
                .perform(get("/auth/signIn").with(httpBasic("user", "password")))
                .andExpect(status().isNotFound())
                .andExpect(authenticated().withUsername("user"));

    }

    @Configuration
    @EnableWebMvcSecurity
    @EnableWebMvc
    static class Config extends WebSecurityConfigurerAdapter {
        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .inMemoryAuthentication()
                    .withUser("user").password("password").roles("USER");
        }
    }
}
