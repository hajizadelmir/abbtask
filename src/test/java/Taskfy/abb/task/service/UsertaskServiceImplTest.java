package Taskfy.abb.task.service;

import Taskfy.abb.task.dto.organizationDto.OrganizationInfoDto;
import Taskfy.abb.task.dto.userTaskDto.TaskRequest;
import Taskfy.abb.task.dto.userTaskDto.TaskResponse;
import Taskfy.abb.task.dto.userTaskDto.TaskSearchRequest;
import Taskfy.abb.task.dto.userTaskDto.TaskSearchResponseInfo;
import Taskfy.abb.task.enumeration.TaskStatus;
import Taskfy.abb.task.exception.NotFoundException;
import Taskfy.abb.task.model.Organization;
import Taskfy.abb.task.model.Task;
import Taskfy.abb.task.repository.OrganizationRepository;
import Taskfy.abb.task.repository.TaskRepository;
import Taskfy.abb.task.repository.UserRepository;
import Taskfy.abb.task.service.impl.UserTaskServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.exceptions.misusing.UnfinishedStubbingException;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UsertaskServiceImplTest {
    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final LocalDate DUMMY_DATE = LocalDate.parse("2015-02-20");

    private static final String TASK_TITLE_MUST_BE_UNIQUE = "This name called string has already been used.";

    @InjectMocks
    private UserTaskServiceImpl taskService;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private OrganizationRepository organizationRepository;

    @Spy
    private ModelMapper modelMapper;

    @Captor
    private ArgumentCaptor<Task> csCaptor;

    private TaskSearchRequest taskSearchRequest;
    private TaskRequest taskRequest;

    private TaskResponse taskResponseDto;

    private Organization organization;

    private Task task;


    @BeforeEach
    void setUp() {
        taskRequest = getTaskRequestDto();

        taskResponseDto = getTaskResponseDto();

        organization = createOrganization();

        task = getTask();
    }

    @Test
    void givenTaskRequestDtoWhenCreateThenTaskResponseDto() {

        //Assert
        assertThat(taskRequest.getDescription()).isNotNull();
        assertThat(task.getTitle()).isEqualTo(DUMMY_STRING);
    }

    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenException() {
        //Arrange
        when(taskRepository.findByTitle(anyString()));


        //Act & Assert
        assertThatThrownBy(() -> taskService.createTask(taskRequest))
                .isInstanceOf(UnfinishedStubbingException.class);

    }

    @Test
    void givenGenericSearchDtoWhenSearchThenExceptTaskResponseDto() {
        //Arrange
//        when(taskRepository.findAll(any(SearchSpecification.class), any(Pageable.class)))
//                .thenReturn(Page.empty());

        //Act
//        taskService.search(new GenericSearchDto(), Pageable.unpaged());

        //Assert
        verify(taskRepository, times(1));
//                .findAll(any(SearchSpecification.class), any(Pageable.class));
    }

    @Test
    void givenInValidTaskIdWhenDeleteThenException() {

        //Act & Assert
        assertThatThrownBy(() -> taskService.deleteTask(DUMMY_ID)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void givenValidTaskIdWhenDeleteThenOk() {

        when(taskRepository.findById(any(Long.class))).thenReturn(Optional.of(task));
        //Act
        taskService.deleteTask(DUMMY_ID);

        //Verify
        verify(taskRepository, times(1)).delete(any(Task.class));
    }

    @Test
    public void taskResponseSearchList() {

        //Arrange
        List<Task> companies = new ArrayList<>();

        Pageable pageable = PageRequest.of(0, 5);
        Page<Task> result = new PageImpl<>(companies, pageable, 1);


        when(taskRepository.findAll(getTaskSearchRequest())).thenReturn(result);

        Page<TaskSearchResponseInfo> list = taskService.taskList(taskSearchRequest);
        assertEquals(list.getTotalElements(), result.getTotalElements());

    }

    private Organization createOrganization() {
        return Organization.builder()
                .id(DUMMY_ID)
                .name(DUMMY_STRING)
                .build();
    }

    private TaskResponse getTaskResponseDto() {
        return TaskResponse
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATE)
                .organization(OrganizationInfoDto.builder()
                        .id(DUMMY_ID)
                        .name(DUMMY_STRING)
                        .build())
                .build();
    }

    private Task getTask() {
        return Task
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATE)
                .organization(Organization.builder()
                        .id(DUMMY_ID)
                        .name(DUMMY_STRING)
                        .build())
                .build();
    }

    private TaskRequest getTaskRequestDto() {
        return TaskRequest
                .builder()
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATE)
                .organizationId(DUMMY_ID)
                .build();
    }

    private TaskSearchRequest getTaskSearchRequest() {
        return TaskSearchRequest.builder()
                .name(DUMMY_STRING)
                .build();
    }

}
