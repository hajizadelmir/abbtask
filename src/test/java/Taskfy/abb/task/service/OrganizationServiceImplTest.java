package Taskfy.abb.task.service;

import Taskfy.abb.task.dto.organizationDto.AddUserDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationInfoDto;
import Taskfy.abb.task.dto.organizationDto.OrganizationSearch;
import Taskfy.abb.task.exception.organizationException.OrganizationAlreadyExistException;
import Taskfy.abb.task.exception.organizationException.OrganizationNotFoundException;
import Taskfy.abb.task.mapper.OrganizationMapper;
import Taskfy.abb.task.model.Organization;
import Taskfy.abb.task.model.User;
import Taskfy.abb.task.repository.OrganizationRepository;
import Taskfy.abb.task.repository.UserRepository;
import Taskfy.abb.task.service.impl.OrganizationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OrganizationServiceImplTest {

    private static final String NAME = "ABB";

    private static final Long DUMMY_ID = 1L;

    private static final String ORGANIZATION_NOT_FOUND_MESSAGE = "Organization with id 1 not Found.";

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private OrganizationMapper organizationMapper;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private OrganizationServiceImpl organizationService;

    @Mock
    Organization mockOrg;

    @Mock
    List<User> mocUsers;

    private Organization organization;

    @Mock
    private OrganizationSearch organizationSearch;

    @Mock
    private OrganizationDto organizationDto;

    private OrganizationInfoDto organizationInfoDto;

    private AddUserDto addUserDto;
    private User user;

    @BeforeEach
    void setUp() {
        user = getUser();

        organization = getOrganization();

        organizationSearch =getOrganizationSearchRequestDto();

        organizationDto = getOrganizationRequestDto();

        organizationInfoDto = getOrganizationResponseInfoDto();

        addUserDto = getRegisterOrgUse();

    }
    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenSuccess() {
        //Arrange
        when(organizationRepository.save(organization)).thenReturn(organization);
        when(organizationMapper.organizationToDto(organization)).thenReturn(organizationInfoDto);
        when(organizationRepository.findByName(NAME)).thenReturn(Optional.empty());

        //Act
        OrganizationInfoDto result = organizationService.create(organizationDto);

        //Assert
        assertThat(organizationDto.getName()).isNotNull();
        assertThat(organization.getName()).isEqualTo(NAME);
        assertThat(result.getName()).isEqualTo(NAME);
        assertThat(result.getId()).isEqualTo(1L);
        verify(organizationRepository, times(1)).save(organization);
    }

    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenException() {
        //Arrange
        when(organizationRepository.findByName(any())).thenReturn(Optional.of(organization));


        //Act & Assert
        assertThatThrownBy(() -> organizationService.create(organizationDto))
                .isInstanceOf(OrganizationAlreadyExistException.class);
    }

    @Test
    public void organizationResponseSearchList() {

        //Arrange

        List<Organization> companies = new ArrayList<>();

        Pageable pageable= PageRequest.of(0,5);
        Page<Organization> result = new PageImpl<>(companies,pageable,1);


        when(organizationRepository.findAll(getOrganizationSearchRequestDto())).thenReturn(result);

        Page<OrganizationInfoDto> list = organizationService.list(organizationSearch);
        assertEquals(list.getTotalElements(),result.getTotalElements());

        //Act


    }


    @Test
    void givenInValidIdAndValidServiceAddOrganizationUserWhenThenException() {
        //Arrange
        when(organizationRepository.findById(any())).thenReturn(Optional.of(mockOrg));
        when(mockOrg.getUser()).thenReturn(List.of(user));

        //Act & Assert
        assertThatThrownBy(() -> organizationService.addUserToOrganization(addUserDto, DUMMY_ID))
                .isInstanceOf(OrganizationAlreadyExistException.class);
    }

    @Test
    void givenInValidIdAndValidServiceAddOrganizationUserWhenThenReturnOk() {
        //Arrange
        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        when(organizationRepository.findById(any())).thenReturn(Optional.of(mockOrg));
        when(mockOrg.getUser()).thenReturn(mocUsers);
        when(mocUsers.add(any())).thenReturn(true);

        //Act
        organizationService.addUserToOrganization(addUserDto, DUMMY_ID);
    }


    @Test
    void givenInValidIdAndValidServiceDeleteOrganizationUserOrganizationIdWhenUpdateThenException() {
        //Arrange
        when(organizationRepository.findById(any())).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> organizationService.removeUserFromOrganization(addUserDto, DUMMY_ID))
                .isInstanceOf(OrganizationNotFoundException.class)
                .hasMessage(String.format(ORGANIZATION_NOT_FOUND_MESSAGE, DUMMY_ID));
    }

    @Test
    void givenInValidIdAndValidServiceDeleteOrganizationUserWhenUpdateThenReturnOk() {
        //Arrange
        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        when(organizationRepository.findById(any())).thenReturn(Optional.of(mockOrg));
        when(mockOrg.getUser()).thenReturn(mocUsers);
        when(mocUsers.remove(any())).thenReturn(true);

        //Act
        organizationService.removeUserFromOrganization(addUserDto, DUMMY_ID);
    }


    private OrganizationDto getOrganizationRequestDto() {
        return OrganizationDto.builder()
                .name(NAME)
                .build();
    }

    private Organization getOrganization() {
        return Organization.builder()
                .name(NAME)
                .build();
    }


    private User getUser() {
        return User.builder()
                .id(1L)
                .email("test@gmail.com")
                .build();
    }


    private AddUserDto getRegisterOrgUse() {
        return AddUserDto.builder()
                .userId(1L)
                .build();
    }

    private OrganizationInfoDto getOrganizationResponseInfoDto() {
        return OrganizationInfoDto.builder()
                .id(1L)
                .name(NAME)
                .build();
    }

    private OrganizationSearch getOrganizationSearchRequestDto() {
        return OrganizationSearch.builder()
                .id(1L)
                .name("sss")
                .build();
    }

}
